//
//  IntExt.swift
//  The Movie DB Exercise
//
//  Created by Prawira Hadi Fitrajaya on 30/11/22.
//

import Foundation

extension Int {
    
    func numberSeparator() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = ","
        
        return formatter.string(from: self as NSNumber) ?? ""
    }
}
