//
//  MovieDetailsCellViewModel.swift
//  The Movie DB Exercise
//
//  Created by Prawira Hadi Fitrajaya on 30/11/22.
//

import Foundation

struct MovieDetailsCellViewModel {
    var authorName: String
    var reviewRating: Double
    var authorReview: String
}
